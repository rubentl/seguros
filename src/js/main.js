window.onload = function() {

    function diaSemana() {
        var getFecha = new Date();
        var diasSemana = new Array("Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado");
        return diasSemana[getFecha.getDay()];
    };

    function elMes() {
        var getFecha = new Date();
        var meses = new Array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        return meses[getFecha.getMonth()];
    };

    function fecha() {
        var f = new Date();
        return diaSemana() + ", " + f.getDate() + " de " + elMes() + " de " + f.getFullYear();
    };

    function animarSol(svgDoc, velocidad) {
        var sol = svgDoc.getElementById('sol');
        var valorSolY = 180;
        var valorSolX = -110;
        var intervalo = setInterval(function() {
            sol.setAttribute('transform', 'translate(' + valorSolX +',' + valorSolY + ')');
            valorSolY = valorSolY + 1;
            valorSolX = valorSolX + 1.5;
            if (valorSolY> 380) {
                clearInterval(intervalo);
            };
        }, velocidad);
    };

    function respuestaEmail(){
        var url = window.location.href;
        var dialogo = url.split('#').pop();
        if (dialogo == 'gracias-mail'){
            $('#gracias-mail').modal('open');
        }else if (dialogo == 'error-mail'){
            $('#error-mail').modal('open');
        };
    }

    //Inicio los dialogos modales para los formularios.
    $('#formulario__coche, #formulario__hogar, #formulario__comercio').modal({
        induration:1500,
        outDuration:800,
        dismisible:true
    });
    // y para el envío del email
    $('#error-mail, #gracias-mail').modal({
        induration:1500,
        outDuration:800,
        dismisible:true
    });
    // la espera por defecto para los tooltip
    $('.material-icons.tooltipped').tooltip({
        delay:0
    });
    //el tooltip legal
    $('.tooltipped.legal').tooltip({
        delay:0,
        html: true,
        position: 'top',
        tooltip: '<span class="legal__tooltipped"> De conformidad con lo establecido en la Ley 15/1999, de 13 de diciembre, de Protección de Datos de Carácter Personal, le informamos de que todos los datos facilitados serán incorporados a un fichero cuya titularidad corresponde a José María Díaz-Romeral Martiarena, inscrito an la D.G.S.Y.F.P. con la clave AF0055, agente de seguros vinculado con domicilio en c/ La Ronda nº 11, código postal 39700 en Castro Urdiales y con teléfono/fax: 942863154.<br/> A su vez, y en virtud de lo establecido en la Ley 34/2002, de 11 de julio, de Servicios de la Sociedad de la Información y de Comercio electrónico el titular le informa que podrá utilizar las direcciones de correo electrónico facilitadas, para remitirle información acerca de sus productos y servicios, avisos y ofertas y, en general, información de carácter comercial de interés relativa a la actividad de la empresa.<br/> Podrá ejercer su derecho de acceso, rectificación, cancelación y oposición al tratamiento de sus datos personales, en los términos y condiciones previstos en la normativa vigente, mediante comunicación remitida a la siguiente dirección de correo electrónico: <a href="mailto:info@ofertaseguroscastro.com">info@ofertaseguroscastro.com</a></span>'
    });

    $('<p>' + fecha() + '</p>').appendTo('#fecha');
    var svg = document.querySelector("#fondo");
    var svgDoc = svg.contentDocument;

    //animación del amanecer en el fondo
    animarSol(svgDoc, 100);

    //evitar que los checkbox estén sin valor al enviar el formulario
    $('form').submit(function(event){
        $("#hogar__garaje,#hogar__trastero,#comercio__verja,#comercio__extintor,#comercio__alarma").each(function(){
            if (!$(this).prop('checked')){
                $(this).val('No');
                $(this).prop('checked', true);
            }; 
        });
        return; 
    });

    $('#hogar__construccion').on('focusout', function(){
        $('#hogar__reforma').prop('disabled', true);
        if ($(this).val() !== '' && parseInt($(this).val()) < 1980) {
                $('#hogar__reforma').prop('disabled', false);
            };
    });
    // activar el modal de respuesta al envio del correo
    // según sea error o exito
    respuestaEmail();
};
