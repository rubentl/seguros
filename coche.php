<?php

require "config.php";

$hoy = getdate();

$fecha = 'Fecha: '.$hoy['mday'].'.'.$hoy['mon'].'.'.$hoy['year'];

$mensaje = "<!DOCTYPE html><html><head><title>Seguro de auto</title></head>";
$mensaje .= "<body><h1>Seguro de Auto</h1><h3>".$fecha."</h3><blockquote>";
$mensaje .= "<p><strong>Nombre y Apellidos: </strong> ".$_POST['coche__nombre']." </p>";
$mensaje .= "<p><strong>D.N.I.: </strong>".$_POST['coche__dni']."</p>";
$mensaje .= "<p><strong>Email: </strong>".$_POST['coche__email']."</p>";
$mensaje .= "<p><strong>Teléfono: </strong>".$_POST['coche__telefono']."</p>";
$mensaje .= "<p><strong>Fecha de nacimiento: </strong> ".$_POST['coche__nacimiento']." </p>";
$mensaje .= "<p><strong>Fecha de carnet: </strong> ".$_POST['coche__carnet']." </p>";
$mensaje .= "<p><strong>Marca del auto: </strong> ".$_POST['coche__marca']." </p>";
$mensaje .= "<p><strong>Modelo del auto: </strong> ".$_POST['coche__modelo']." </p>";
$mensaje .= "<p><strong>Cilindrada: </strong> ".$_POST['coche__cilindrada']." </p>";
$mensaje .= "<p><strong>Caballos: </strong> ".$_POST['coche__caballos']." </p>";
$mensaje .= "<p><strong>Número de puertas: </strong> ".$_POST['coche__puertas']." </p>";
$mensaje .= "<p><strong>Matrícula: </strong> ".$_POST['coche__matricula']." </p>";
$mensaje .= "<p><strong>Cobertura: </strong> ".$_POST['coche__cobertura']." </p>";
$mensaje .= "<p><strong>Número de póliza: </strong> ".$_POST['coche__poliza']." </p>";
$mensaje .= "<p><strong>Código postal: </strong> ".$_POST['coche__postal']." </p>";
$mensaje .= "<p><strong>Términos legales: </strong> ".$_POST['coche__legal']." </p>";
$mensaje .= "<blockquote></body></html>";

$mail->Body = $mensaje;

$dir_subida = 'uploads/';
$fichero_subido = $dir_subida . basename($_FILES['coche__file']['name']);
if (!is_dir($dir_subida)) { mkdir($dir_subida, 0775);}
move_uploaded_file($_FILES['coche__file']['tmp_name'], $fichero_subido);

$mail->addAttachment($fichero_subido);

if(!$mail->send()) {
    header('Location: /index.html#error-mail');
} else {
    header('Location: /index.html#gracias-mail');
}
exit();

?>
