#!/usr/bin/python

from livereload import Server, shell

server = Server()

server.watch('src/css/*.scss',
             shell('sass -t compressed src/css/estilos.scss '
                   'css/estilos.min.css'))
server.watch('src/js/*.js',
             shell('babili src/js/main.js -o js/main.min.js'))
server.watch('img/fondo.svg',
             shell('xmllint --format -o fondo-1.svg fondo.svg'))
server.watch('./index.html')

server.serve(port=8080, host='localhost')
