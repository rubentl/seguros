<?php
require "config.php";

$hoy = getdate();

$fecha = 'Fecha: '.$hoy['mday'].'.'.$hoy['mon'].'.'.$hoy['year'];

$mensaje = "<!DOCTYPE html><html><head><title>Seguro de comercio</title></head>";
$mensaje .= "<body><h1>Seguro de Comercio</h1><h3>".$fecha."</h3><blockquote>";
$mensaje .= "<p><strong>Nombre y Apellidos: </strong> ".$_POST['comercio__nombre']." </p>";
$mensaje .= "<p><strong>D.N.I.: </strong>".$_POST['comercio__dni']."</p>";
$mensaje .= "<p><strong>Email: </strong>".$_POST['comercio__email']."</p>";
$mensaje .= "<p><strong>Teléfono: </strong>".$_POST['comercio__telefono']."</p>";
$mensaje .= "<p><strong>Código postal: </strong> ".$_POST['comercio__postal']." </p>";
$mensaje .= "<p><strong>Año de construcción: </strong> ".$_POST['comercio__construccion']." </p>";
$mensaje .= "<p><strong>Año de reforma: </strong> ".$_POST['comercio__reforma']." </p>";
$mensaje .= "<p><strong>Metros cuadrados: </strong> ".$_POST['comercio__m2']." </p>";
$mensaje .= "<p><strong>Verja: </strong> ".$_POST['comercio__verja']." </p>";
$mensaje .= "<p><strong>Extintor: </strong> ".$_POST['comercio__extintor']." </p>";
$mensaje .= "<p><strong>Alarma: </strong> ".$_POST['comercio__alarma']." </p>";
$mensaje .= "<p><strong>Contenido existencias: </strong> ".$_POST['comercio__existencias']." </p>";
$mensaje .= "<p><strong>Contenido mobiliario: </strong> ".$_POST['comercio__mobiliario']." </p>";
$mensaje .= "<p><strong>Contenido maquinaria: </strong> ".$_POST['comercio__maquinaria']." </p>";
$mensaje .= "<p><strong>Número de póliza: </strong> ".$_POST['comercio__poliza']." </p>";
$mensaje .= "<p><strong>Actividad: </strong> ".$_POST['comercio__actividad']." </p>";
$mensaje .= "<p><strong>Términos legales: </strong> ".$_POST['comercio__legal']." </p>";
$mensaje .= "<blockquote></body></html>";

$mail->Body = $mensaje;

$dir_subida = 'uploads/';
$fichero_subido = $dir_subida . basename($_FILES['comercio__file']['name']);
if (!is_dir($dir_subida)) { mkdir($dir_subida, 0775);}
move_uploaded_file($_FILES['comercio__file']['tmp_name'], $fichero_subido);

$mail->addAttachment($fichero_subido);

if(!$mail->send()) {
    header('Location: /index.html#error-mail');
} else {
    header('Location: /index.html#gracias-mail');
}
exit();

?>
