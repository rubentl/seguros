<?php
require "config.php";

$hoy = getdate();

$fecha = 'Fecha: '.$hoy['mday'].'.'.$hoy['mon'].'.'.$hoy['year'];

$mensaje = "<!DOCTYPE html><html><head><title>Seguro de hogar</title></head>";
$mensaje .= "<body><h1>Seguro de Hogar</h1><h3>".$fecha."</h3><blockquote>";
$mensaje .= "<p><strong>Nombre y Apellidos: </strong> ".$_POST['hogar__nombre']." </p>";
$mensaje .= "<p><strong>D.N.I.: </strong>".$_POST['hogar__dni']."</p>";
$mensaje .= "<p><strong>Email: </strong>".$_POST['hogar__email']."</p>";
$mensaje .= "<p><strong>Teléfono: </strong>".$_POST['hogar__telefono']."</p>";
$mensaje .= "<p><strong>Código postal: </strong> ".$_POST['hogar__postal']." </p>";
$mensaje .= "<p><strong>Año de construcción: </strong> ".$_POST['hogar__construccion']." </p>";
if (isset($_POST['hogar__reforma'])) {
    $mensaje .= "<p><strong>Año de reforma: </strong> ".$_POST['hogar__reforma']." </p>";
}
$mensaje .= "<p><strong>Metros cuadrados: </strong> ".$_POST['hogar__m2']." </p>";
$mensaje .= "<p><strong>Garaje: </strong> ".$_POST['hogar__garaje']." </p>";
$mensaje .= "<p><strong>Trastero: </strong> ".$_POST['hogar__trastero']." </p>";
$mensaje .= "<p><strong>Capital contenido: </strong> ".$_POST['hogar__capital']." </p>";
$mensaje .= "<p><strong>Número de póliza: </strong> ".$_POST['hogar__poliza']." </p>";
$mensaje .= "<p><strong>Términos legales: </strong> ".$_POST['hogar__legal']." </p>";
$mensaje .= "<blockquote></body></html>";

$mail->Body = $mensaje;

$dir_subida = 'uploads/';
$fichero_subido = $dir_subida . basename($_FILES['hogar__file']['name']);
if (!is_dir($dir_subida)) { mkdir($dir_subida, 0775);}
move_uploaded_file($_FILES['hogar__file']['tmp_name'], $fichero_subido);

$mail->addAttachment($fichero_subido);

if(!$mail->send()) {
    header('Location: /index.html#error-mail');
} else {
    header('Location: /index.html#gracias-mail');
}
exit();

?>
